<?php

// Zorg dat de variabelen $voornaam en $achternaam de juiste inhoud krijgen.
// Plak de variabelen aan elkaar met de string-concatenatie-operator en zet het antwoord in $naam.
// Toon het resultaat op de gerenderde html-pagina.

$voornaam = "Jos";
$achternaam = "Janssens";

$naam = $voornaam . ' ' . $achternaam; // Vergeet de spatie niet!

echo $naam;

?>
