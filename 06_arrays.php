<?php

// Maak een Array met 3 namen van medeleerlingen.
$namen = ["Jos", "Jan", "Mieke"];

// Itereer er over met een for-loop. Print ze af in een HTML ongenummerde lijst.
echo "<ul>";
for($i=0; $i<count($namen); $i++) {
    echo "<li>" . $namen[$i] . "</li>";
}
echo "</ul>";

// Itereer er over met een foreach-loop. Print ze af in HTML genummerde lijst.
echo "<ul>";
foreach($namen as $naam) {
    echo "<li>" . $naam . "</li>";
}
echo "</ul>";


$naam = "Willy";
// Itereer met een for-loop over de variable $naam alsof het een array was.
// TIP: foreach werkt niet met een string
echo "<ul>";
for($i = 0; $i < strlen($naam); $i++) {
    echo "<li>" . $naam[$i] . "</li>";
}
echo "</ul>";

?>
