<?php

$waarde = 3;
$Waarde = 8; // let op de hoofdletter W!

if($waarde == 3) {
    // Als $waarde nog steeds 3 is, dan is $Waarde een andere variabele
    echo "Variable-names are case-sensitive";
    if($Waarde == 8) {
        echo '<br>And indeed... $Waarde is a new variabele, with value: ' . $Waarde;
    }
} else if($waarde == 8) {
    // Als het statement '$Waarde = 8', $waarde veranderd zou hebben, dan zijn...
    echo "Variable-names are case-insensitive";
    // ... maar dat is dus niet zo!
}

// Demonstratie van single (') en double (") quotes.
// Double quotes evalueren een variabele. Single quote niet;
// tussen single quotes is $ een gewoon karakter.

echo "<br>";
echo '$waarde';
echo "<br>";
echo "$waarde";
echo "<br>";

if($val == null) {
    echo "$val is not defined.";
}

echo "<br>";

// Print de string "De waarde van $waarde is 8." door maar 1 keer gebruik te maken v.d. concatentie-operator en de juiste soort quotes: single (') of double (").

echo 'De waarde van $waarde is ' . "$waarde.";
// of
echo 'De waarde van $waarde is ' . $waarde; // geen punt op het einde v.d. zin hier

?>
